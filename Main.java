public class Main {
	
	public static void main(String []args){
		Counter theCounter = new Counter();
		
		theCounter.increaseValue();
		theCounter.increaseValue();
		theCounter.increaseValue();
		
		//System.out.println("the counter value is " + theCounter.getValue());
		theCounter.printValue();
		
		theCounter.resetValue();
		
		//System.out.println("the counter value is " + theCounter.getValue());
		theCounter.printValue();
	}
}
