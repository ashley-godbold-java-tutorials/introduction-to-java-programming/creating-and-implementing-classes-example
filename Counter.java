public class Counter {
	private int value;
	
	public int getValue() {
		return value;
	}
	
	public void increaseValue() {
		value = value + 1;
	}
	
	public void resetValue() {
		value = 0;
	}
	
	public void printValue() {
		System.out.println("the counter value is " + getValue());
	}
}
